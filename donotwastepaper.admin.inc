<?php

/**
 * @file
 * Admin configuration
 */

/**
 * Display admin configuration form.
 */
function donotwastepaper_admin_form($form, &$form_state) {
  $form = array();
  $form['donotwastepaper'] = array(
    '#type' => 'textfield',
    '#title' => t('Do not waste paper message'),
    '#description' => t('This message will be shown on the print page in browser.'),
    '#default_value' => variable_get('donotwastepaper', t('Do not waste paper!')),
  );

  return system_settings_form($form);
}
