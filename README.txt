
Do not waste paper module
-------------------------
by Sven Culley, contact@sense-design.de


Description
-----------
The do not waste paper module displays a message on the print page in browser.
Our days there is no need to print a whole website with all its graphics,
sidebars and colors. If you like to print data from a website, copy and paste
the needed data to an office text editor and continue with further editing and
printing.


Features
--------
* Displays a message on the print page (customizable)
* Hides website content completely
* Website user is not able to print the entire website via browser


Install
-------
Just activate the module


Configuration
-------------
The print message can be customized under "admin/config/system/donotwastepaper"
